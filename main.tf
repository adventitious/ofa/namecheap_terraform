terraform {
  required_providers {
    namecheap = {
      source = "namecheap/namecheap"
      version = ">= 2.0.0"
    }
  }
}

variable "NAMECHEAP_USERNAME" {
  type = string
}

variable "NAMECHEAP_API_USER" {
  type = string
}

variable "NAMECHEAP_API_KEY" {
  type = string
}

variable "NAMECHEAP_CLIENT_IP" {
  type = string
}

variable "NAMECHEAP_USE_SANDBOX" {
  type = bool
}

provider "namecheap" {
  user_name = var.NAMECHEAP_USERNAME
  api_user = var.NAMECHEAP_API_USER
  api_key = var.NAMECHEAP_API_KEY
  client_ip = var.NAMECHEAP_CLIENT_IP
  use_sandbox = var.NAMECHEAP_USE_SANDBOX
}

resource "namecheap_domain_records" "onefreeapp-com" {
  domain = "onefreeapp.com"
  mode = "MERGE"

  record {
    type = "A"
    hostname = "onefreeapp.com"
    address = "162.255.119.191"
    ttl = 1799
  }

  record {
    type = "CNAME"
    hostname = "www"
    address = "onefreeapp.com"
  }
}
